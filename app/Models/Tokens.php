<?php

namespace App\Models;

use Illuminate\Database\Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tokens
 *
 * @package App\Models
 * @mixin Eloquent\Model
 * @property int $id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tokens whereUpdatedAt($value)
 */
class Tokens extends Model
{
    protected $fillable = [
        'remember_token',
    ];

    use HasFactory;
}
