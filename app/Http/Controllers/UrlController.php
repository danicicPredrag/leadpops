<?php

namespace App\Http\Controllers;

use App\Http\Requests\createURLRequest;
use App\Http\Traits\TokenTrait;
use App\Models\Tokens;
use App\Models\Url;
use Illuminate\Support\Str;

class UrlController extends Controller
{
    use TokenTrait;

    public function createUrlAction(createURLRequest $request)
    {
        $data = $request->validated();

        $token = Tokens::first();
        // this could be done with rules
        $isExpired = $this->isExpired($token);

        if ($isExpired) {
            return "token expierd";
        }

        $url = Url::firstOrNew(['actual_url' => $data['url']]);

        if (empty($url->shorten_url)) {
            $url->shorten_url = Str::random(6);// There should be 36^6 combinations ... so I ignored possible duplications
            $url->save();
        }

        return [
            'shorten_url' => url('/api') . '/' . $url->shorten_url,
        ];
    }

    public function redirectAction(Url $url) {
        if (!empty($url->actual_url)) {
            return redirect($url->actual_url);
        }
    }
}
