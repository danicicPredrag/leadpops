<?php

namespace App\Http\Controllers;

use App\Http\Traits\TokenTrait;
use App\Models\Tokens;

class ApiTokenController extends Controller
{
    use TokenTrait;
    /**
     * Update the authenticated user's API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function createOrUpdateAction()
    {
        // Laravel-ide-helper did not helped me to remove this "yellow method" at the end I gave up for now
        $token = Tokens::first();

        /**
         * Method isExpired() is not necessary
         * All this chould be done with Repository and eloquent model or query ( this solution would probably be better )
         * But in task it is written that I should not use "external packages" and repository is not native in laravel ( as far as I know ) ..
         * This is reason we are using Trait here I do not want to make complicated query with logic on multiple places ..
         *
         * It also could be done with "Requests class" but I did not wanted to dive deep in that now ..
         */
        $isExpired = $this->isExpired($token);

        if ($isExpired) {
            $token_string = bin2hex(random_bytes(32));
            if (empty($token)) {
                $token = new Tokens;
                $token->remember_token = $token_string;
                $token->save();
            } else {
                $token = tap($token)->update([
                    'remember_token' => $token_string,
                ]);
            }
        }

        return ['token' => $token->remember_token];

    }
}
