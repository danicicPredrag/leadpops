<?php

namespace App\Http\Traits;


use Carbon\Carbon;

trait TokenTrait
{
    /**
     * @param $token
     * @return bool
     */
    public function isExpired($token): bool
    {
        if (empty($token)) {
            return true;
        }
        $updated_at = $token->updated_at;
        $created_at = $token->created_at;

        $now = Carbon::now();

        // $diff_updated_at will be 0 when $updated_at is null;
        $diff_updated_at = $now->diffInMinutes($updated_at);
        $diff_created_at = $now->diffInMinutes($created_at);

        if ($diff_created_at > 10 && $diff_updated_at === null) {
            return true;
        }

        if ($diff_updated_at > 10) {
            return true;
        }

        return false;
    }
}
