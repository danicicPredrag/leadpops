<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiTokenController;
use App\Http\Controllers\UrlController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/greeting', function () {
    return 'Hello World';
});

Route::get(
    '/createOrUpdateToken',
    [ApiTokenController::class, 'createOrUpdateAction']
)->name('createOrUpdateToken');

Route::post(
    '/createShortenUrl',
    [UrlController::class, 'createUrlAction']
)->name('createOrUpdateToken');

Route::get('/{url:shorten_url}', [UrlController::class, 'redirectAction']);

