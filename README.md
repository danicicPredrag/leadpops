##When I was working on this task I did not removed any of laravel existing models or migrations 


###I left them there .. so there are some "user" and "password" files that are not needed 

### Originally my plan was to treat users as tokens
### And to use api authentication for createing new tokens and refreshing it 
### However there was problems with registration of "tokens" and refreshing time
### So I gave up that idea and switched to "stupid approach"
### I used Laravel Ide-helper in this task ( and this was only external laravel libbary I was using )

### In order to start program do those things
1. Go to .env file and set database password
2. Navigate to project dir and execute composer update ( I think that vendor files are not uploaded) 
3. Execute migations
4. http://127.0.0.1:8000/api/createOrUpdateToken -> is route for creating token ( get )
5. http://127.0.0.1:8000/api/createShortenUrl -> is route for creating shorten "url string" need to send with json data 
   {
   "token":"{token_value}",
   "url":"https://google.com"
   }
   
   In order to make this work in normaly headers Accept:application/json && Content-Type:application/json needs to be set
   
6.  http://127.0.0.1:8000/api/{shorten_url}  for redirect
